package com.epam.learn.l2;

public class Logic {
    boolean paramA = true;
    boolean paramB = false;
    boolean paramC = true;

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.checkResult(logic.paramA, logic.paramB));
        System.out.println(logic.checkResult2(logic.paramC, logic.paramB));
        System.out.println(logic.checkResult3(logic.paramA, logic.paramB, logic.paramC));
    }

    public boolean checkResult(boolean a, boolean b){
        return a && b;
    }

    public boolean checkResult2(boolean c, boolean b){
        return c || b;
    }

    public boolean checkResult3(boolean a, boolean b, boolean c){
        return a || b && c;
    }
}
