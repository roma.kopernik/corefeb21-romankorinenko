package com.epam.learn.kata;

public class KataTriangleArea {
    public static void main(String[] args) {
        System.out.print(floatArea(".\n"));
        System.out.print(" " + floatArea(".\n. .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n. . . . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n. . . . . . . .\n"));
        System.out.print(" " + floatArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n. . . . . . . .\n. . . . . . . . .\n"));
        System.out.println("\n");
        System.out.print(floatArea(".\n"));
        System.out.print(" " + tArea(".\n. .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n. . . . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n. . . . . . . .\n"));
        System.out.print(" " + tArea(".\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .\n. . . . . . . .\n. . . . . . . . .\n"));
    }

    static float floatArea(String str){
        String[] strings = str.split("\n");
        if (strings.length == 1 && ".".equals(strings[0])) return 0f;
        if (strings.length == 2 && ".".equals(strings[0]) && ". .".equals(strings[1])) return 0.5f;
        int a = (strings.length - 2);
        int b = strings[strings.length -2].replaceAll(" ", "").length() - 1;
        float res = (float)a * b / 2;
        return res;
    }

    public static float tArea(String tStr) {
        String[] strings = tStr.split("\n");
        int a = strings.length - 2;
        return (float) a * a / 2;
    }
}
