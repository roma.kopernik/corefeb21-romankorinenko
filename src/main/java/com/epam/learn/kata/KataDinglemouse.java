package com.epam.learn.kata;

public class KataDinglemouse {
    public static String replaceDots(final String str) {
        return str.replaceAll(".", "-");
    }

    public static void main(String[] args) {
        System.out.println(replaceDots("n.n.n.n-n-n-n"));
    }
}
