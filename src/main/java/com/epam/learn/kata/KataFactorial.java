package com.epam.learn.kata;

import java.math.BigInteger;

public class KataFactorial {
    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    private static String factorial(int n) {
        if (n <= 0) throw new IllegalArgumentException();
        BigInteger s = BigInteger.valueOf(1);
        for (long i = 2; i <= n; i++){
            BigInteger i2 = BigInteger.valueOf(i);
            s = s.multiply(i2);
        }
        return s.toString();
    }
}
