package com.epam.learn.kata;

import java.util.Arrays;
import java.util.stream.Stream;

import static java.lang.Math.*;

public class KataAreaOfACircle {
    public static void main(String[] args) {
        System.out.println(area(5));
        System.out.println(howManyDalmatians(5));
        System.out.println(arrayPlusArray(new int[]{1, 2, 3}, new int[]{4, 5, 6}));
    }

    private static int arrayPlusArray(int[] ints, int[] ints2) {
        return Stream.of(ints, ints2).flatMapToInt(Arrays::stream).sum();
//        return Arrays.stream(ints).sum() + Arrays.stream(ints2).sum();
    }

    private static double area(int i) {
        if (i < 0) throw new IllegalArgumentException("false");
        else if ( i == 0) return 0;
        double radius = PI * i * i;
        return radius;
    }

    public static String howManyDalmatians(int number) {

        String[] dogs = {"Hardly any", "More than a handful!", "Woah that's a lot of dogs!", "101 DALMATIANS!!!"};

        String respond = number <= 10 ? dogs[0] : number <= 50 ? dogs[1] : number == 101 ?  dogs[3] : dogs[2];

        return respond;
    }

}
