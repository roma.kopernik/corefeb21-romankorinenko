package com.epam.learn.kata;

import java.util.ArrayList;
import java.util.List;

public class KataSimplePigLatin {
    public static String simplePigLatin(String str) {
        StringBuilder res = new StringBuilder();
        String[] strArr = str.split("\\s++");
        List<String> list = new ArrayList<>();
        for (String s : strArr) {
            list.add(pigify(s));
        }
        for (String s: list) {
            res.append(s + " ");
        }
        return res.toString().trim();
    }

    static String pigify(String str) {
        String reg = "\\W+|\\s+";
        String lastS = "";
        String clear = "";
        String res = "";

        if (str.length()==1 && str.matches(reg)) {
            res = str;
        } else if (str.length() >=1 && (!str.matches(reg))) {
            res = str.substring(1) + str.charAt(0) + "ay" + lastS;
        }

        if (str.length()>=2 ) {
            if(String.valueOf(str.charAt(str.length()-1)).matches(reg)){
                lastS = str.substring(str.length() - 1);
            }
            if(String.valueOf(str.charAt(str.length()-2)).matches(reg)){
                lastS = str.substring(str.length() - 2);
            }
            clear = str.replaceAll(reg, " ").trim();
            res = clear.substring(1) + clear.charAt(0) + "ay" + lastS;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(simplePigLatin("Pig latin is cool"));
        System.out.println(simplePigLatin("Hello world !"));
    }
}
