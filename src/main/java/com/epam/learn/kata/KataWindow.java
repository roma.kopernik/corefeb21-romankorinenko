package com.epam.learn.kata;

public class KataWindow {
    public static String makeAWindow(int num){
        String stringLast = "---" + "--".repeat(num);
        String stringFirst = stringLast + "\n";
        String stringMiddle = "|" + "-".repeat(num) + "+" + "-".repeat(num) + "|" + "\n";
        String stringWindow = "|" + ".".repeat(num) + "|" + ".".repeat(num) + "|" + "\n";

        return stringFirst + stringWindow.repeat(num) + stringMiddle + stringWindow.repeat(num) + stringLast;
    }

    public static void main(String[] args) {
        String makeAWindow = makeAWindow(3);
        System.out.println(makeAWindow);
    }
}
