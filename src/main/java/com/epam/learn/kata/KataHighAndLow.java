package com.epam.learn.kata;

public class KataHighAndLow {
    public static String highAndLow(String numbers) {

        String[] array = numbers.split(" ");
        int max = -1000;
        int min = 1000;
        for(String arr : array){
            if(max < Integer.parseInt(arr)) max = Integer.parseInt(arr);
            if(min > Integer.parseInt(arr)) min = Integer.parseInt(arr);
        }
        return "" + max + " " + min;
    }

    public static void main(String[] args) {
        System.out.println(highAndLow("1 3 6 -3 -4 0 10"));
    }
}
