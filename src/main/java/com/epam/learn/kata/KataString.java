package com.epam.learn.kata;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KataString {
    public static String generateShape(int n) {
        return IntStream.range(0, n)
                .mapToObj(c -> "+".repeat(n)).collect(Collectors.joining("\n"));
    }

    public static void main(String[] args) {
        String str = generateShape(3);
        System.out.println(str);
    }
}
