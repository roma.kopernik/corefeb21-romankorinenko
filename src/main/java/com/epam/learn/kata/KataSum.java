package com.epam.learn.kata;

public class KataSum {
    public static int digital_root(int n) {
        // ...
        int result = 0;
        String str = String.valueOf(n);

        for(int i = 0; i < str.length(); i++){
            result += Integer.parseInt(String.valueOf(str.charAt(i)));
        }

        if (result == 10) return 1;
        if (result < 10) return result;
        return digital_root(result);

    }

    public static void main(String[] args) {
       System.out.println(digital_root(12345));
    }
}
