package com.epam.learn.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorldDao {

    public static void main(String[] args) {
        List<City> cityList = null;

        try  {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            Connection conn = databaseConnection.getConnection();
            Statement statement = conn.createStatement();

            String query = "SELECT Name, CountryCode FROM world.city LIMIT 10;";
            ResultSet rs = statement.executeQuery(query);
           cityList = new ArrayList<>();
            while(rs.next()){
                cityList.add(new City(rs.getString("Name"), rs.getString("CountryCode")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(cityList != null) {
            cityList.forEach(System.out::println);
        }
    }


    }





