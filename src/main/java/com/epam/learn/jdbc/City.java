package com.epam.learn.jdbc;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class City {
    String name;
    String abbreviatedName;


}
