package com.epam.learn.l1;

import java.util.*;
import java.util.stream.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;

public class MainMain {

    public static void main(String[] args) {
        int [] array = {5, 4, 5, 4};
        System.out.println(getAverage(array));
        System.out.println(getAverageMark(new int[]{5, 4, 5, 4}));
    }

    private static int[] solve(int[] ints) {
        return Stream.of(ints)
                .flatMap(r -> Arrays.stream(r).boxed())
                .collect(Collectors.groupingBy(identity(), counting()))
                .entrySet().stream()
                .filter(value -> value.getValue() == 1)
                .map(Map.Entry::getKey)
                .mapToInt(w -> w)
                .toArray();
    }

    static int getAverage(int [] marks){
        return IntStream.of(marks).sum() / marks.length;
    }

    static int getAverageMark(int [] marks){
        return (int) IntStream.of(marks).average().getAsDouble();
    }
}
