package com.epam.learn.l3;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class VectorMain {
    public static void main(String[] args) {
        Vector<String> vct1 = new Vector<String>();
        vct1.add("One");
        vct1.add("Two");
        vct1.add("Three");
        vct1.add("Four");
        vct1.add("Five");
        vct1.add("Six");
        Iterator itr = vct1.iterator();
        vct1.remove(0);
        System.out.println(itr.next()); //ConcurrentModificationException

        Vector<String> vct = new Vector<String>();
        vct.add("One");
        vct.add("Two");
        vct.add("Three");
        vct.add("Four");
        vct.add("Five");
        vct.add("Six");
        Enumeration enm = vct.elements();
        vct.remove(0);
        System.out.println(enm.nextElement()); // Two
    }
}
