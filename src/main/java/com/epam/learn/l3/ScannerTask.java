package com.epam.learn.l3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerTask {

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            int a = scan.nextInt();
            int b = scan.nextInt();
            System.out.println(a / b);
        } catch (ArithmeticException e){
            System.out.println(e);
        } catch (InputMismatchException ex){
            System.out.println(ex.getClass().getName());
        }
    }
}
