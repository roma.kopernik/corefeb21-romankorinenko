package com.epam.learn.l3;

import java.time.*;

public class Main {
    public static void main(String[] args) {
        LocalDate montyPythonDay = LocalDate.of(2017, Month.MAY, 10);
        LocalTime time = LocalTime.of(5, 40);
        LocalDateTime dateTime = LocalDateTime.of(montyPythonDay, time);
        Duration duration = Duration.ofDays(1);
        LocalDateTime result = dateTime.minus(duration);
        System.out.println(result);


        int first = Integer.parseInt("5");
        int second = Integer.valueOf("5");

    }
}
