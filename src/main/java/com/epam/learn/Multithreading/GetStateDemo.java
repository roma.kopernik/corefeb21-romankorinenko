package com.epam.learn.Multithreading;

import java.util.concurrent.TimeUnit;

public class GetStateDemo implements Runnable{
    public static void main(String[] args) {
        Thread thread = new Thread(new GetStateDemo());
        thread.start();
        thread.interrupt();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Thread.State state = thread.getState();
        System.out.println(thread.getName() + " " + state);
    }

    @Override
    public void run() {
       Thread.State state = Thread.currentThread().getState();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " " + state);
    }
}
